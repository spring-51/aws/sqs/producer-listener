package com.sqs.controller;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sqs.dto.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/sqs")
public class SqsController {

    @Autowired
    private AmazonSQS sqs;

    @Value("${cloud.aws.end-point.uri}")
    private String endPoint;

    // sqs producers

    @GetMapping("/producer")
    public Student sendMessage(@RequestParam("msg") String msg) {

        Student student = new Student();
        student.setId(1l);
        student.setName("Name");
        student.setRollNumber("0103EI101028");
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = mapper.writeValueAsString(student);
            // way1
            //queueMessagingTemplate.send(endPoint, MessageBuilder.withPayload(jsonString).build());

            // org.springframework.messaging.Message<String> build = MessageBuilder.withPayload(jsonString).build();
            SendMessageRequest request = new SendMessageRequest(endPoint, msg);
            request.setMessageGroupId("group1");
            request.setMessageDeduplicationId(msg);
            sqs.sendMessage(request);

            // uncomment below to test listner nanually
            // [WORKING]
            /*
            ReceiveMessageResult receiveMessageResult = sqs.receiveMessage(endPoint);
            System.out.println(receiveMessageResult);

             */
        } catch (Exception e) {
            e.printStackTrace();
        }
        return student;
    }

    // this API is exposed if we don't want @SqsListener to auto read from queue
    @GetMapping("/consumer")
    public String receiveMessageManually(){
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest();
        receiveMessageRequest.setQueueUrl(endPoint);
        // [Working]
        // ReceiveMessageResult receiveMessageResult = sqs.receiveMessage(endPoint);
        List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();

        if(!messages.isEmpty()){
            for (Message m : messages) {
                System.out.println(m.getBody());
                sqs.deleteMessage(endPoint, m.getReceiptHandle());
            }
            return "check logs" ;
        }else{
            return "no messages left" ;
        }


    }
    // sqs listeners
    @SqsListener(value = "${cloud.aws.stack.sqs.queue-name}")
    public void receiveMessageAutomatically(String stringJson) throws IOException {
        System.out.println("############## - "+stringJson);
    }

}
